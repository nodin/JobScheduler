#JobScheduler
支持的功能：
 1. 按指定间隔时间和超时来执行任务
 2. 任务状态被持久化到 Redis 中，当系统重启时支持继续运行
 3. 通过 Redis 的原子操作来实现互斥，防止一个任务被多个实例执行

 实现：
 1. 使用 ScheduledExecutor 来实现定时任务
 2. 针对每一个任务都用一个 redis hash 来保存状态，包含三个字段：state, class 和 data
 3. state 代表状态，如果存在这个字段且值为 running，代表有人在执行这个任务
 4. class 和 data 用来持久任务的类型和数据。其中任务数据的保存和恢复需要 Job 子类自行实现（saveState 和 restoreState 方法）
 5. 每次任务被执行后，如果需要继续调度，其 saveState 会被调用，新的状态会被保存到 redis 中
 6. 一个 JobScheduler 启动时会从 redis 中加载未完成的任务继续执行